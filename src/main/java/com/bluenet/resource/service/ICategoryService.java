package com.bluenet.resource.service;

import com.bluenet.resource.model.Category;

import java.util.List;

public interface ICategoryService {

    List<Category> getAllCategories();

    Category getCategory(Long catId);
}
