package com.bluenet.resource.service.impl;

import com.bluenet.resource.dao.CategoryRepo;
import com.bluenet.resource.exception.ResourceNotFoundException;
import com.bluenet.resource.model.Category;
import com.bluenet.resource.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryService implements ICategoryService {

    @Autowired
    private CategoryRepo catRepo;

    @Override
    public List<Category> getAllCategories(){
        Iterable<Category> itr = catRepo.findAll();

        // Create an empty Collection to hold the result
        List<Category> categories = new ArrayList<Category>();

        // Iterate through the iterable to
        // add each element into the collection
        for (Category c : itr)
            categories.add(c);

        if (categories.size() == 0) {
            throw new ResourceNotFoundException();
        }

        return categories;

    }

    @Override
    public Category getCategory(Long catId){
        Optional<Category> singleCategory = catRepo.findCategoryById(catId);

        if (!singleCategory.isPresent()) {
            throw new ResourceNotFoundException();
        }

        return singleCategory.get();
    }

}
