package com.bluenet.resource.service.impl;

import com.bluenet.resource.dao.NewsRepo;
import com.bluenet.resource.exception.ResourceNotFoundException;
import com.bluenet.resource.model.News;
import com.bluenet.resource.service.INewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class NewsService  implements INewsService {

    @Autowired
    private NewsRepo newsRepo;

    @Override
    public List<News> getAllNews() {
        Iterable<News> itr = newsRepo.findAll();

        // Create an empty Collection to hold the result
        List<News> news = new ArrayList<News>();

        // Iterate through the iterable to
        // add each element into the collection
        for (News n : itr)
            news.add(n);

        if (news.size() == 0) {
            throw new ResourceNotFoundException();
        }

        return news;
    }

    @Override
    public News getNews(Long newsId) {
        Optional<News> singleNews = newsRepo.findNewsById(newsId);

        if (!singleNews.isPresent()) {
            throw new ResourceNotFoundException();
        }

        return singleNews.get();
    }

    @Override
    public List<News> getNewsByCategory(String categoryName) {

        List<News> news = newsRepo.findNewsByCategory(categoryName);

        if (news.size() == 0) {
            throw new ResourceNotFoundException();
        }

        return news;
    }

}
