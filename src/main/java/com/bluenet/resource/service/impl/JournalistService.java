package com.bluenet.resource.service.impl;

import com.bluenet.resource.dao.JournalistRepo;
import com.bluenet.resource.exception.ResourceNotFoundException;
import com.bluenet.resource.model.Journalist;
import com.bluenet.resource.service.IJournalistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JournalistService implements IJournalistService {

    @Autowired
    private JournalistRepo journalistRepo;

    @Override
    public List<Journalist> getAllJournalists(){
        Iterable<Journalist> itr = journalistRepo.findAll();

        // Create an empty Collection to hold the result
        List<Journalist> journalists = new ArrayList<Journalist>();

        // Iterate through the iterable to
        // add each element into the collection
        for (Journalist c : itr)
            journalists.add(c);

        if (journalists.size() == 0) {
            throw new ResourceNotFoundException();
        }

        return journalists;

    }

    @Override
    public Journalist getJournalist(Long journalistId){
        Optional<Journalist> singleJournalist = journalistRepo.findJournalistById(journalistId);

        if (!singleJournalist.isPresent()) {
            throw new ResourceNotFoundException();
        }

        return singleJournalist.get();
    }
}
