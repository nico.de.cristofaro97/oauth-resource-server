package com.bluenet.resource.service.impl;

import com.bluenet.resource.dao.AgentRepo;
import com.bluenet.resource.exception.ResourceNotFoundException;
import com.bluenet.resource.model.Agent;
import com.bluenet.resource.service.IAgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AgentService implements IAgentService {

    @Autowired
    private AgentRepo agentRepo;

    @Override
    public List<Agent> getAgents() {
        Iterable<Agent> itr = agentRepo.findAll();

        // Create an empty Collection to hold the result
        List<Agent> agents = new ArrayList<Agent>();

        // Iterate through the iterable to
        // add each element into the collection
        for (Agent t : itr)
            agents.add(t);

        if (agents.size() == 0) {
            throw new ResourceNotFoundException();
        }

        return agents;
    }

    @Override
    public Agent getAgent(String agentCode) {
        Optional<Agent> agent = agentRepo.findByAgentCode(agentCode);

        if (!agent.isPresent()) {
            throw new ResourceNotFoundException();
        }

        return agent.get();
    }

    @Override
    public List<Agent> getAgentsByDepartment(String department) {

        List<Agent> agents = agentRepo.findAgentByDepartment(department);

        if (agents.size() == 0) {
            throw new ResourceNotFoundException();
        }

        return agents;
    }
}
