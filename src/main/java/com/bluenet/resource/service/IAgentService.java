package com.bluenet.resource.service;

import com.bluenet.resource.model.Agent;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IAgentService {

    List<Agent> getAgents();

    Agent getAgent(String agentCode);

    List<Agent> getAgentsByDepartment(String department);

}