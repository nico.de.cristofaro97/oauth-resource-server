package com.bluenet.resource.service;

import com.bluenet.resource.model.News;

import java.util.List;

public interface INewsService {

    List<News> getAllNews();

    News getNews(Long newsId);

    List<News> getNewsByCategory(String categoryName);
}
