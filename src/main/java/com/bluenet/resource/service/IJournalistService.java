package com.bluenet.resource.service;

import com.bluenet.resource.model.Journalist;

import java.util.List;

public interface IJournalistService {

    List<Journalist> getAllJournalists();

    Journalist getJournalist(Long journalistId);
}
