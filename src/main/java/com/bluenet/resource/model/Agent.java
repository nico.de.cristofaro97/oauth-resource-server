package com.bluenet.resource.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity(name="agent")
@Getter
@Setter
@NoArgsConstructor
public class Agent {

    @Id
    @Column(name="agent_code")
    private String agentCode;

    @Column(name="agent_name")
    private String agentName;

    @Column(name="working_area")
    private String workingArea;

    @Column(name="experience_years")
    private Integer experienceYears;

    @Column(name="phone_no")
    private String phoneNo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "dep_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    private Department department;



}
