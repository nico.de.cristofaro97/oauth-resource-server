package com.bluenet.resource.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity(name="news")
@Getter
@Setter
@NoArgsConstructor
public class News {

    @Id
    @Column(name="news_id")
    private Long id;

    @Column(name="news_image")
    private String image;

    @Column(name="news_title")
    private String title;

    @Column(name="news_short_desc")
    private String shortDescription;

    @Column(name="news_description")
    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    private Category category;

}
