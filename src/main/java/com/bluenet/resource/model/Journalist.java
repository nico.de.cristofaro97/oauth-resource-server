package com.bluenet.resource.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="journalist")
@Getter
@Setter
@NoArgsConstructor
public class Journalist {

    @Id
    @Column(name="journalist_id")
    private Long id;

    @Column(name="journalist_name")
    private String name;

    @Column(name="journalist_surname")
    private String surname;

    @Column(name="journalist_workingArea")
    private String workingArea;

    @Column(name="journalist_experienceYears")
    private Integer experienceYears;
    
}
