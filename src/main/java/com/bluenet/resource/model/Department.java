package com.bluenet.resource.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="department")
@Getter
@Setter
@NoArgsConstructor
public class Department {

    @Id
    @Column(name="dep_id")
    private String depId;

    @Column(name="dep_name")
    private String depName;

    @Column(name="dep_central_location")
    private String centralLocation;

}
