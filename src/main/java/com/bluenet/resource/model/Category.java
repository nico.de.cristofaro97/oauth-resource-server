package com.bluenet.resource.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="category")
@Getter
@Setter
@NoArgsConstructor
public class Category {

    @Id
    @Column(name="category_id")
    private Long id;

    @Column(name="category_name")
    private String name;

    @Column(name="category_description")
    private String description;
}
