package com.bluenet.resource.dao;

import com.bluenet.resource.model.News;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NewsRepo  extends CrudRepository<News,Long> {

    Optional<News> findNewsById(Long newsId);

    @Query("SELECT n FROM news n WHERE category.name = :catName")
    List<News> findNewsByCategory(@Param("catName") String catName);

}
