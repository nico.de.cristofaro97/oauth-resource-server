package com.bluenet.resource.dao;

import com.bluenet.resource.model.Journalist;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface JournalistRepo extends CrudRepository<Journalist,Long> {

    Optional<Journalist> findJournalistById(Long journalistId);
}
