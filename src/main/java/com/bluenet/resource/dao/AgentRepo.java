package com.bluenet.resource.dao;

import com.bluenet.resource.model.Agent;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AgentRepo extends CrudRepository<Agent,String> {

    Optional<Agent> findByAgentCode(String agentCode);

    @Query("SELECT a FROM agent a WHERE department.depName = :dep")
    List<Agent> findAgentByDepartment(@Param("dep") String department);

}