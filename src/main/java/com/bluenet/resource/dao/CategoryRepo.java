package com.bluenet.resource.dao;

import com.bluenet.resource.model.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepo extends CrudRepository<Category,Long> {

    Optional<Category> findCategoryById(Long catId);
}
