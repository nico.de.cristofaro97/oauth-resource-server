package com.bluenet.resource.controller;

import com.bluenet.resource.model.Agent;
import com.bluenet.resource.service.IAgentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class AgentController {

    @Autowired
    private IAgentService agentService;

    private static final Logger logger = LoggerFactory.getLogger(AgentController.class);

    //Retrieve some random but protected resources from database (example secret agents)
    @RequestMapping(path="/agents",method = GET)
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_CLIENT')")
    public List<Agent> getAgents(){
        logger.info("[REST] REQUEST GET_AGENTS ");
        List<Agent> results = agentService.getAgents();
        logger.info("[REST] RESULT GET_AGENTS", results.size());
        return results;
    }

    @RequestMapping("/agents/{id}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_CLIENT')")
    public Agent getAgent(@PathVariable(value = "id", required = true) String agentCode) {
        logger.info("[REST] REQUEST GET_AGENT for agentCode [{}] ", agentCode);
        Agent result = agentService.getAgent(agentCode);
        logger.info("[REST] RESULT GET_AGENT for agentCode [{}] - found:[{}]",agentCode, result != null);
        return result;
    }

    @RequestMapping(path="/agents/department",method = GET)
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_CLIENT')")
    public List<Agent> getAgentsByDepartment(@RequestParam(value="department") String department){
        logger.info("[REST] REQUEST GET_AGENTS BY DEPARTMENTS");
        List<Agent> results = agentService.getAgentsByDepartment(department);
        logger.info("[REST] RESULT GET_AGENTS BY DEPARTMENT", results.size());
        return results;
    }

}

