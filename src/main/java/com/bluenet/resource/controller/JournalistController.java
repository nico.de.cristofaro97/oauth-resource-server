package com.bluenet.resource.controller;

import com.bluenet.resource.model.Journalist;
import com.bluenet.resource.service.IJournalistService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class JournalistController {

    @Autowired
    private IJournalistService journalistService;

    private static final Logger logger = LoggerFactory.getLogger(AgentController.class);

    @RequestMapping(path = "/journalist", method = GET)
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_CLIENT')")
    public List<Journalist> getAllJournalists() {
        logger.info("[REST] REQUEST GET_ALL_JOURNALISTS ");
        List<Journalist> results = journalistService.getAllJournalists();
        logger.info("[REST] RESULT GET_ALL_JOURNALISTS", results.size());
        return results;
    }

    @RequestMapping("/journalist/{id}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_CLIENT')")
    public Journalist getJournalist(@PathVariable(value = "id", required = true) Long jourId) {
        logger.info("[REST] REQUEST GET_SINGLE JOURNALIST for jourId [{}] ", jourId);
        Journalist result = journalistService.getJournalist(jourId);
        logger.info("[REST] RESULT GET_SINGLE JOURNALIST for jourId [{}] - found:[{}]",jourId, result != null);
        return result;
    }
}
