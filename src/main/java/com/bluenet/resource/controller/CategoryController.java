package com.bluenet.resource.controller;

import com.bluenet.resource.model.Category;
import com.bluenet.resource.service.ICategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class CategoryController {

    @Autowired
    private ICategoryService categoryService;

    private static final Logger logger = LoggerFactory.getLogger(AgentController.class);

    @RequestMapping(path = "/category", method = GET)
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_CLIENT')")
    public List<Category> getAllCategories() {
        logger.info("[REST] REQUEST GET_ALL_CATEGORIES ");
        List<Category> results = categoryService.getAllCategories();
        logger.info("[REST] RESULT GET_ALL_CATEGORIES", results.size());
        return results;
    }

    @RequestMapping("/category/{id}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_CLIENT')")
    public Category getCategory(@PathVariable(value = "id", required = true) Long catId) {
        logger.info("[REST] REQUEST GET_SINGLE CATEGORY for catId [{}] ", catId);
        Category result = categoryService.getCategory(catId);
        logger.info("[REST] RESULT GET_SINGLE CATEGORY for catId [{}] - found:[{}]",catId, result != null);
        return result;
    }
}
