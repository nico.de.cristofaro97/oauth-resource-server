package com.bluenet.resource.controller;

import com.bluenet.resource.model.News;
import com.bluenet.resource.service.INewsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class NewsController {

    @Autowired
    private INewsService newsService;

    private static final Logger logger = LoggerFactory.getLogger(AgentController.class);

    @RequestMapping(path = "/news", method = GET)
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_CLIENT')")
    public List<News> getAllNews() {
        logger.info("[REST] REQUEST GET_ALL_NEWS ");
        List<News> results = newsService.getAllNews();
        logger.info("[REST] RESULT GET_ALL_NEWS", results.size());
        return results;
    }

    @RequestMapping("/news/{id}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_CLIENT')")
    public News getNews(@PathVariable(value = "id", required = true) Long newsId) {
        logger.info("[REST] REQUEST GET_SINGLE NEWS for newsId [{}] ", newsId);
        News result = newsService.getNews(newsId);
        logger.info("[REST] RESULT GET_SINGLE_NEWS for newsId [{}] - found:[{}]",newsId, result != null);
        return result;
    }

    @RequestMapping(path="/news/category",method = GET)
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_CLIENT')")
    public List<News> getNewsByCategory(@RequestParam(value="category") String catName){
        logger.info("[REST] REQUEST GET_NEWS BY CATEGORY");
        List<News> results = newsService.getNewsByCategory(catName);
        logger.info("[REST] RESULT GET_NEWS BY CATEGORY", results.size());
        return results;
    }
}
