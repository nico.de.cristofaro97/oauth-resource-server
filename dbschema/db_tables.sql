-- Here you can find a DB schema for some "NEWS" protected resources to be accessed by the resource server. (Simulating "La Repubblica" website)
DROP DATABASE IF EXISTS resources;

CREATE DATABASE resources;

USE resources;

drop table if exists category;
create table category (
    category_id    int auto_increment,
    category_name  varchar(50),
    category_description varchar(100),
    primary key(category_id)
);

INSERT INTO category VALUES
(NULL, "Tech", "News about innovation and new tech gadgets"),
(NULL, "Finance", "News about finance and economy");

drop table if exists news;
CREATE TABLE  news (
    news_id INTEGER NOT NULL auto_increment,
    news_image varchar(500),
    news_title CHAR(100),
    news_short_desc VARCHAR(250),
    news_description varchar(2500),
    category_id int,
    Primary Key (news_id),
    Foreign Key (category_id) References category(category_id)
    On Update cascade On Delete cascade
 );

INSERT INTO news VALUES
(NULL,"https://icdn3.digitaltrends.com/image/digitaltrends/sony-wh-1000x-m3-headphones-review-10.jpg","Sony: cancellazione rumore estrema","Le cuffie Sony 1000X M3 over-ear a cancellazione di rumore sono la terza iterazione della già eccezionale serie 1000X di Sony. I 1000X originali costarono un centesimo sotto i $ 400 e furono immediatamente tra i migliori della loro categoria. La Sony sembrava trovare poco spazio ...","The Sony 1000X M3 over-ear, noise-canceling headphones are the third iteration of Sony’s already great 1000X series. The original 1000Xs cost a cent under $400 and were instantly among the best in their class. Sony seemed to find little room for improvement with its second-gen 1000X M2s, so it chopped $100 off the price and polished up a couple of technical aspects — though that came at the cost of some excitement in their sound. Just as it seemed as though Sony would slip behind the rapidly improving competition, however, the 1000X M3s arrive and rectify almost every issue the series has had so far, while splitting the cost difference between its predecessors with a sensible $349 price.Sony got two things very right with its first-gen 1000X headphones: the noise canceling and the fit. That’s why I find it surprising that the company has gone for a major redesign with its M3 generation: the physical design didn’t seem in need of much tweaking. But everything that Sony has changed has been for the better. It takes courage to tinker with a popular design and skill to actually improve on it.
Some 1000X M1 and M2 users had complained about the headband being susceptible to cracking (an issue I never encountered in months of contented use of those headphones), and they’ll be comforted to know Sony’s design shakeup has delivered a new headband with more padding.",1),

(NULL,"https://cnet2.cbsistatic.com/img/TMBumavV-oqsYE13Gbw_VesWZag=/1092x0/2019/07/26/febbe65e-845b-4e8d-992a-e7b62754499b/iphone11-fan-2.jpg","Apple presenta i nuovi IPhone","Quest'anno, Apple ha presentato i suoi nuovi iPhone XI. Apple ha attraversato tutti i numeri da uno a dieci, saltando due e nove per le sue ragioni e quest'anno ci ha stupiti con il suo degign particolare dato dalla fotocamera posteriore ...","This year, Apple reached the end of its iPhone X (read: ten) names. Next year remains a mystery. Apple has gone through all the numbers from one through ten, skipping two and nine for its own reasons. Now, for better or worse, next year, it’ll likely need to figure out a new naming scheme.
Apple hastened its way toward a dead end last year when it decided to switch to Roman numerals rather than continuing with numbers as it had done in the past. A report from Bloomberg at the end of August indicated that the people behind the names struggled to come up with names that would work. Since this year’s iPhones didn’t really change many things beyond last year’s, they didn’t seem compelling enough to grant a whole new name. The cheapest iPhone is also bigger than the midrange option, so Apple namers had to take that into consideration, too and aim for a name that wouldn’t confuse consumers.
The final results still seem... kind of bad. For some reason, Apple went with an iPhone XS Max, instead of just going for an iPhone XS and an iPhone Max. The distinction is clear: Apple wanted to send a message that the two phones are related in some way. But to the average person, the iPhone XS Max just sounds like a pretty bad name. Why didn’t they go with iPhone XS and XL? That just makes logical sense, and since we’re used to seeing those letters represent sizes, it’s not as unfamiliar and difficult to say as XR and XS Max and XS. Try saying that five times fast.",1),

(NULL,"https://o.aolcdn.com/images/dims?quality=85&image_uri=https%3A%2F%2Fo.aolcdn.com%2Fimages%2Fdims%3Fcrop%3D1600%252C914%252C0%252C0%26quality%3D85%26format%3Djpg%26resize%3D1600%252C914%26image_uri%3Dhttps%253A%252F%252Fs.yimg.com%252Fos%252Fcreatr-uploaded-images%252F2019-11%252F377bdad0-0e39-11ea-b7f6-6c4ad76696bd%26client%3Da1acac3e1b3290917d92%26signature%3D7534bc1e364bd114fa7844cf20794ca1676cee34&client=amp-blogside-v2&signature=0f6bb707922aef180ab376b8211c4b14b75a71bd","Google rivela il suo smartphone: Pixel 4","Conosciamo già bene le performance date dai predecessori Pixel 3 e Pixel 3 XL in termini di produttività e ora abbiamo grosse aspettative per il nuovo smartphone di punta di Google, il Pixel 4...","We’re already well familiar with what the Pixel 3 and Pixel 3 XL look like in terms of design, and now we know what colors Google’s next flagship smartphones will come in.
Droid Life noticed this Google-hosted, confetti-filled “coming soon” teaser website that shows an outline of a Pixel phone with the company’s G logo at the bottom. Clicking on the logo rotates between three different colors: mint, white, and black. The green hue inside the white outline is almost certainly a reference to the colored buttons on the white and mint Pixels; the black phone shows gray instead of the green.
Last year’s Pixel 2 came in white, black, and a muted “kinda blue.” So Google isn’t deviating from its three-color approach. But if you were hoping to see something more vibrant or eye-catching out of this year’s lineup, that looks like a no go. Maybe next year, red Pixel.",1),
(NULL,"https://images.samsung.com/is/image/samsung/au-akg-n700nc-gp-n700hahceaa-frontsilver-128121164?$PD_GALLERY_L_JPG$","Samsung AKG wireless headphones","Samsung is releasing three wireless AKG headphones today, a selection of neckbuds, on-ear, and over-the-ear headphones named the Y100, Y500, and N700NC respectively. Samsung is focusing on balancing environmental noise with this series...","Samsung is releasing three wireless AKG headphones today, a selection of neckbuds, on-ear, and over-the-ear headphones named the Y100, Y500, and N700NC respectively. Samsung is focusing on balancing environmental noise with this series. The Y100s and Y500s both feature “Ambient Aware” technology that lets users hear surrounding sound, while the NC700NCs have adaptive noise canceling that lets listeners control the amount of surrounding sound they want to hear.
Prices range from $99.95 for the Y100 neckbuds to $149.95 for the Y500 on-ear headphones to $349.95 for the more premium noise-canceling over-the-ear N700NCs. Although the N700NC headphones are the most expensive of the bunch, besides the adaptive noise canceling, the Y500 headphones bring more features. The Y500s can automatically pause music when the headphones are taken off, and they have a better 33-hour battery life compared to 20 hours on the N700NCs. The Y100s, in comparison, have eight hours of battery life. The Y500s have a slightly longer charging time of 2.5 hours to the Y100 and N700NC’s two hours, though.",1);


drop table if exists journalist;
create table journalist (
    journalist_id int auto_increment,
    journalist_name varchar(30),
    journalist_surname varchar(30),
    journalist_workingArea VARCHAR(35),
    journalist_experienceYears INTEGER,
    primary key(journalist_id)
);

INSERT INTO journalist VALUES
(NULL, "Nicola","De Cristofaro","Salerno",3);

