-- Here you can find a DB schema for some random(at the moment) protected resources to be accessed by the resource server.
DROP DATABASE IF EXISTS resources;

CREATE DATABASE resources;

USE resources;

CREATE TABLE IF NOT EXISTS  department (
    dep_id INTEGER NOT NULL PRIMARY KEY,
    dep_name CHAR(40),
    dep_central_location VARCHAR(35)
 );

INSERT INTO department VALUES ('1001', 'Kidnappings', 'Brisbane');
INSERT INTO department VALUES ('1002', 'Murders', 'Melbourne');
INSERT INTO department VALUES ('1003', 'Terrorism', 'Naples');
INSERT INTO department VALUES ('1004', 'Internal affairs', 'London');
INSERT INTO department VALUES ('1005', 'Finance', 'Paris');

CREATE TABLE IF NOT EXISTS  agent (
    agent_code CHAR(4) NOT NULL,
    agent_name VARCHAR(40),
    working_area VARCHAR(35),
    experience_years INTEGER,
    phone_no CHAR(15),
    dep_id INTEGER,
    Primary Key (agent_code),
    Foreign Key (dep_id) References department(dep_id)
    On Update cascade On Delete cascade
 );

INSERT INTO agent VALUES ('007', 'Ramasundar', 'Bangalore', '10', '077-25814763', '1001');
INSERT INTO agent VALUES ('003', 'Alex ', 'London', '13', '075-12458969', '1002');
INSERT INTO agent VALUES ('008', 'Alford', 'New York', '12', '044-25874365', '1003');
INSERT INTO agent VALUES ('011', 'Ravi Kumar', 'Bangalore', '15', '077-45625874', '1004');
INSERT INTO agent VALUES ('010', 'Santakumar', 'Chennai', '14', '007-22388644', '1005');
INSERT INTO agent VALUES ('012', 'Lucida', 'San Jose', '12', '044-52981425', '1001');
INSERT INTO agent VALUES ('005', 'Anderson', 'Brisbane', '13', '045-21447739', '1002');
INSERT INTO agent VALUES ('001', 'Subbarao', 'Bangalore', '14', '077-12346674', '1003');
INSERT INTO agent VALUES ('002', 'Mukesh', 'Mumbai', '10', '029-12358964', '1004');
INSERT INTO agent VALUES ('006', 'McDen', 'London', '6', '078-22255588', '1005');
INSERT INTO agent VALUES ('004', 'Ivan', 'Torento', '4', '008-22544166', '1001');
INSERT INTO agent VALUES ('009', 'Benjamin', 'Hampshair', '8', '008-22536178', '1002');

